﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TunerMovie.Models
{
    public class MoviesViewModel
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        //public int? TitleTypeId { get; set; }
        //public string TitleTypeDesc { get; set; }
        public int? ReleaseYear { get; set; }

        public ICollection<Award> Awards { get; set; }
        public ICollection<OtherName> OtherNames { get; set; }
        public ICollection<StoryLine> StoryLines { get; set; }
        public ICollection<TitleGenre> TitleGenres { get; set; }
        public ICollection<TitleParticipant> TitleParticipants { get; set; }
        public ICollection<ParticipantViewModel> Participants { get; set; }
        public ICollection<Genre> Genres { get; set; }
    }
}