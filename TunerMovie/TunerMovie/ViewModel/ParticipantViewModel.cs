﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TunerMovie.Models
{
    public class ParticipantViewModel
    {
        public string Name { get; set; }
        public string ParticipantType { get; set; }
        public string RoleType { get; set; }
    }
}