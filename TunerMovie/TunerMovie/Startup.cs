﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TunerMovie.Startup))]
namespace TunerMovie
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
