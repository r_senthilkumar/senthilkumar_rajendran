﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TunerMovie.Models;

namespace TunerMovie.Controllers
{
    public class MovieController : Controller
    {
        //TitlesEntitiesNew _db;
        TitlesEntities _db;
        public MovieController()
        {
            //_db = new TitlesEntitiesNew();
            _db = new TitlesEntities();
        }

        public ActionResult Index(string searchString)
        {
            if (string.IsNullOrEmpty(searchString)) searchString = string.Empty;
            var titles = _db.Titles.Where(t => t.TitleName.Contains(searchString)).ToList();
            //var viewModel = titles.Select(t => GetViewModel(t)).ToList();

            return View(titles);
        }
       
        public ActionResult Details(int? Id = null)
        {
            if (Id != null)
            {
                var title = _db.Titles.Single(t => t.TitleId == Id);

                return View(GetViewModel(title));
            }
            ModelState.AddModelError("Error", "Please select the movie from movie list.");
            return View();
        }

        private MoviesViewModel GetViewModel(Title title)
        {

            var _genres = _db.TitleGenres
                .Where(tg => tg.TitleId == title.TitleId)
                .Join(_db.Genres, tg => tg.GenreId, g => g.Id, (tg, g) => g).ToList();

            var _participants = _db.TitleParticipants
                .Where(tp => tp.TitleId == title.TitleId)
                .Join(_db.Participants, tp => tp.ParticipantId, p => p.Id, (tp, p) => new ParticipantViewModel
                {
                    Name = p.Name,
                    ParticipantType = p.ParticipantType,
                    RoleType = tp.RoleType
                }).ToList();

            return new MoviesViewModel
            {
                TitleId = title.TitleId,
                TitleName = title.TitleName,
                ReleaseYear = title.ReleaseYear,
                //TitleTypeId = title.TitleTypeId,
                //TitleTypeDesc = "Movie Name",
                Awards = _db.Awards.Where(a => a.TitleId == title.TitleId).ToList(),
                OtherNames = _db.OtherNames.Where(ons => ons.TitleId == title.TitleId).ToList(),
                StoryLines = _db.StoryLines.Where(ons => ons.TitleId == title.TitleId).ToList(),
                Genres = _genres,
                Participants = _participants
            };


        }
    }
}